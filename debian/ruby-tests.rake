require 'gem2deb/rake/spectask'

if RUBY_VERSION.to_f >= 3.1
  ENV['RUBYOPT'] = "#{ENV['RUBYOPT']} --disable-error_highlight"
end

Gem2Deb::Rake::RSpecTask.new(:spec) do |t|
  t.pattern = FileList['./spec/**/*_spec.rb']
end

task :default => :spec

